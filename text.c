
#include<REG52.H>
#include <stdio.h>
#include <stdlib.h>
#define seg_data P3

sbit LED_BLUE = P0^5;
sbit LED_RED = P0^6;
sbit LED_YELLOW = P0^7;

sbit seg1 = P1^7;
sbit seg2 = P1^6;
sbit seg3 = P1^5;
sbit seg4 = P1^4;

sbit P1_3 = P1^3;
sbit RS = P1^0;
sbit E = P1^1;

void cct_init(void);
void lcdinit(void);
void returnHome(void);

int but=0, ones=0, tens=0, hundreds=0, thousends=0, duplicount=0, count=0, l, i, j;
int numbers[4] = {1,2,3,4};

void checkLastWin(); void showLastWin(); void CLEAR_DISPLAY(); void DOUBLE()
void TRIPLE(); void BIG_WIN_ALL_SAME(); void NOTHING(); void display_digit(int)
void writedata(char); voide writecmd(int)
void main()
{
	P0 = 0x00;
	LED_BLUE = 1;
	LED_RED = 1;
	LED_YELLOW = 1;
	
	cct_init();

	lcdinit();
	
	writecmd(0x81);
		writedata('W');
		writedata('E');
		writedata('L');
		writedata('C');
		writedata('O');
		writedata('M');
		writedata('E');
		writedata(' ');
		writedata('T');
		writedata('O');
		writedata(' ');
		writedata('T');
		writedata('H');
		writedata('E');
		writedata(' ');
		writedata('G');
		writedata('A');
		writedata('M');
		writedata('E');

	returnHome();

	seg1 = seg2 = seg3 = seg4 = 1;

	while(1)
	{
		if (P1_3 == 0 && but == 0)
		{
			l = rand() % (9999 + 1 - 11) + 11;
			delay(10);

			ones = 1%10;
			tens = (1%10)%10;
			hundreds = (1%100)%10;
			thousands = (1%1000)%10;
			checkLastWin();
			showLastWin();

			but = 1;
		}
		if(P1_3 == 1)
		{
			but = 0;
		}
		if(dupliCount == 2)
		{
			LED_BLUE = 0;
			LED_RED = 1;
			LED_YELLOW = 1;
		}
		else if(dupliCount == 3)
		{
			LED_BLUE = 1;
			LED_RED = 0;
			LED_YELLOW = 1;
		}
		else if(dupliCount == 4)
		{
			LED_BLUE = 1;
			LED_RED = 1;
			LED_YELLOW = 0;
		}		
		else
		{
			LED_BLUE = 1;
			LED_RED = 1;
			LED_YELLOW = 1;
		}		
		ones = 1%10;
		tens = (1%10)%10;
		hundreds = (1%100)%10;
		thousands = (1%1000)%10;

		display_digit(ones);
		seg1 = 0;
		delay(10);
		seg1 = 1;

		display_digit(tens);
		seg2 = 0;
		delay(10);
		seg2 = 1;

		display_digit(hundreds);
		seg3 = 0;
		delay(10);
		seg3 = 1;

		display_digit(thousands);
		seg4 = 0;
		delay(10);
		seg4 = 1;
	}
}
void cct_init(void)
{
	P2 = 0x00;	//used as data port
}
void lcdinit(void)
{
	delay(15000);
	writecmd(0x30);
	delay(4500);
	writecmd(0x30);
	delay(300);
	writecmd(0x30);
	delay(650);

	writecmd(0x38);
	writecmd(0x0c);
	writecmd(0x01);
	writecmd(0x06);
}
void returnHome(void)
{
	writecmd(0x02);
	delay(1500);
}
void delay(int a)
{
	int i;
	for(i=0; i<a; i++);
}
void checkLastWin()
{
	dupliCount = 0;
	numbers[0] = thousands;
	numbers[1] = hundreds;
	numbers[2] = tens;
	numbers[3] = ones;

	count=0;

	for(i=0; i<4; i++)
	{
		for(j=0; j<4; j++)
		{
			if(number[i] == number[j])
			{
				count++;
			}
		}
		if(dupliCount < count)
			dupliCount = count;

		count = 0;
	}
}
void showLastWin()
{
	int someInt = 1;
	char str[4];
	sprintf(str, "%d", someInt);
	str[0] = thousends;
	str[1] = hundreds;
	str[2] = tens;
	str[3] = ones;

	CLEAR_DISPLAY();
	if(dupliCount == 2)
	{
		DOUBLE();
	}
	else if(dupliCount == 3)
	{
		TRIPLE();
	}
	else if(dupliCount == 4)
	{
		BIG_WIN_ALL_SAME();
	}
	else
	{
		NOTHING();
	}
}
void CLEAR_DISPLAY()
{
	writecmd(0x01);
}
void DOUBLE()
{
	writecmd(0xc4);
		writedata('D');
		writedata('O');
		writedata('U');
		writedata('B');
		writedata('L');
		writedata('E');
		writedata(' ');
		writedata('W');
		writedata('I');
		writedata('N');
	returnHome();
}
void TRIPLE()
{
	writecmd(0xc4);
		writedata('T');
		writedata('R');
		writedata('I');
		writedata('P');
		writedata('L');
		writedata('E');
		writedata(' ');
		writedata('W');
		writedata('I');
		writedata('N');
	returnHome();
}
void BIG_WIN_ALL_SAME()
{
	writecmd(0xc4);
		writedata('B');
		writedata('I');
		writedata('G');
		writedata(' ');
		writedata('W');
		writedata('I');
		writedata('N');
		writedata(' ');
		writedata('A');
		writedata('L');
		writedata('L');
		writedata(' ');
		writedata('S');
		writedata('A');
		writedata('M');
		writedata('E');
	returnHome();
}
void NOTHING()
{
	writecmd(0xc4);
	writecmd(0xc4);
		writedata('T');
		writedata('R');
		writedata('Y');
		writedata(' ');
		writedata('A');
		writedata('G');
		writedata('A');
		writedata('I');
		writedata('N');
}
void display_digit(int c)
{
	switch(c)
	{
	case 0: 
		seg_data = 0xbf;
		break;
	case 1:
		seg_data = 0x06;
		break;
	case 2:
		seg_data = 0x5b;
		break;
	case 3:
		seg_data = 0x4f;
		break;
	case 4:
		seg_data = 0x66;
		break;
	case 5:
		seg_data = 0x6d;
		break;
	case 6:
		seg_data = 0x7d;
		break;
	case 7:
		seg_data = 0x07;
		break;
	case 8:
		seg_data = 0x7f;
		break;
	case 9:
		seg_data = 0x6f;
		break;
	}
}
void writedata(char t)
{
	RS = 1;
	P2 = t;
	E = 1;
	delay(150);
	E = 0;
	delay(150);
}
voide writecmd(int z)
{
	RS = 0;
	P2 = z;
	E = 1;
	delay(150);
	E = 0;
	delay(150);
}
